package com.xyzbooks.xyzbooks.exception;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

public class EmptyInputException extends RuntimeException{

    public EmptyInputException(String message) {
        super(EmptyInputException.generateMessage(message));
    }

    private static String generateMessage(String message) {
        return message;
    }

}
