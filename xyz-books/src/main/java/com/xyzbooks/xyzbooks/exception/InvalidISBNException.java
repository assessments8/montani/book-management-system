package com.xyzbooks.xyzbooks.exception;

public class InvalidISBNException extends RuntimeException{

    public InvalidISBNException(String message) {

        super(InvalidISBNException.generateMessage(message));
    }

    private static String generateMessage(String message) {
        return message;
    }

}
