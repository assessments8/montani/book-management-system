package com.xyzbooks.xyzbooks.repositories;

import com.xyzbooks.xyzbooks.models.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    Book findByIsbn13(String isbn13);
}
