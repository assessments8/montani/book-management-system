package com.xyzbooks.xyzbooks.repositories;

import com.xyzbooks.xyzbooks.models.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {
}
