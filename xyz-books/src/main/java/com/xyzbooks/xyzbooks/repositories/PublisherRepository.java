package com.xyzbooks.xyzbooks.repositories;

import com.xyzbooks.xyzbooks.models.Publisher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
