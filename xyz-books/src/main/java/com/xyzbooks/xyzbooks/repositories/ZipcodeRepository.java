package com.xyzbooks.xyzbooks.repositories;

import com.xyzbooks.xyzbooks.models.Zipcode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipcodeRepository extends CrudRepository<Zipcode, Long> {
}
