package com.xyzbooks.xyzbooks.services;

import com.xyzbooks.xyzbooks.dto.mapper;
import com.xyzbooks.xyzbooks.dto.requestDto.BookRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.BookResponseDto;
import com.xyzbooks.xyzbooks.exception.*;
import com.xyzbooks.xyzbooks.models.Author;
import com.xyzbooks.xyzbooks.models.Book;
import com.xyzbooks.xyzbooks.models.Category;
import com.xyzbooks.xyzbooks.models.Publisher;
import com.xyzbooks.xyzbooks.repositories.BookRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorService authorService;
    private final CategoryService categoryService;
    private final PublisherService publisherService;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository,
                           AuthorService authorService,
                           CategoryService categoryService,
                           PublisherService publisherService) {
        this.bookRepository = bookRepository;
        this.authorService = authorService;
        this.categoryService = categoryService;
        this.publisherService = publisherService;
    }

    @Transactional
    @Override
    public BookResponseDto addBook(BookRequestDto bookRequestDto) {
        //write a code to check for their nullity in one expression
        //stream.of is not working
//        if(Stream.of(
//                bookRequestDto.getTitle(),
//                bookRequestDto.getPrice(),
//                bookRequestDto.getPublisherId(),
//                bookRequestDto.getPublicationYear()
//        ).anyMatch(arg -> arg == null )){
//            throw new EmptyInputException();
//        }

        Book book = new Book();
        book.setTitle(bookRequestDto.getTitle());

        if(!isValidIsbn13(bookRequestDto.getIsbn13())) { throw new InvalidISBNException("Invalid ISBN-13"); }
        book.setIsbn13(bookRequestDto.getIsbn13());

        book.setPrice(bookRequestDto.getPrice());
        book.setImgUrl(bookRequestDto.getImgUrl());

        if(bookRequestDto.getPublicationYear() == 0 ) { throw new EmptyInputException("Publication year missing"); }
        book.setPublicationYear(bookRequestDto.getPublicationYear());
        Publisher publisher = publisherService.getPublisher(bookRequestDto.getPublisherId());
        book.setPublisher(publisher);
        Category category = categoryService.getCategory(bookRequestDto.getCategoryId());
        book.setCategory(category);

        if(bookRequestDto.getAuthorIds().isEmpty()){ throw new EmptyInputException("Should have at least one Author"); }
        List <Author> authors = new ArrayList<>();
        for (Long authorId : bookRequestDto.getAuthorIds()){
            Author author = authorService.getAuthor(authorId);
            authors.add(author);
        }
        book.setAuthors(authors);

        Book book1 = bookRepository.save(book);
        return mapper.bookToBookResponseDto(book1);
    }

    @Override
    public BookResponseDto getBookById(Long bookId) {
        Book book = getBook(bookId);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto getBookByIsbn(String bookIsbn) {
        Book book = getBookIsbn13(bookIsbn);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public Book getBook(Long bookId) {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(()->
                     new EntityNotFoundException(Book.class,"id", bookId.toString())
                );
        return book;
    }

    @Override
    public Book getBookIsbn13(String bookIsbn13) {
        //todo:
        //check validity of isbn first
        if(!isValidIsbn13(bookIsbn13)) {
            throw new InvalidISBNException("Invalid ISBN-13");
        }

        Book book = bookRepository.findByIsbn13(bookIsbn13);

        if (book == null) {
            throw new EntityNotFoundException(Book.class, "isbn", bookIsbn13.toString());
        }
        return book;
    }

    @Override
    public List<BookResponseDto> getBooks() {
        List<Book> books = StreamSupport
                .stream(bookRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        return mapper.booksToBookResponseDtos(books);
    }

    @Override
    public BookResponseDto deleteBook(Long bookId) {
        Book book = getBook(bookId);
        System.out.println(book.getId());
        bookRepository.delete(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Transactional
    @Override
    public BookResponseDto editBook(Long bookId, BookRequestDto bookRequestDto) {
        Book bookToEdit = getBook(bookId);

        // book
        if(!bookRequestDto.getTitle().isEmpty() || !bookRequestDto.getTitle().isBlank()) {
            bookToEdit.setTitle(bookRequestDto.getTitle());
        }

        // author
        if (!bookRequestDto.getAuthorIds().isEmpty()) {
            List<Author> authors = new ArrayList<>();
            for (Long authorId : bookRequestDto.getAuthorIds()) {
                Author author = authorService.getAuthor(authorId);
                authors.add(author);
            }
            bookToEdit.setAuthors(authors);
        }

        // isbn
        if(!isValidIsbn13(bookRequestDto.getIsbn13())) {
            throw new InvalidISBNException("Invalid ISBN-13");
        } else {
            bookToEdit.setIsbn13(bookRequestDto.getIsbn13());
        }

        //check price
        if (bookRequestDto.getPrice() != null) {
            bookToEdit.setPrice(bookRequestDto.getPrice());
        } else {
            throw new EmptyInputException("Price missing");

        }

        // category
        if (bookRequestDto.getCategoryId() != null) {
            Category category = categoryService.getCategory(bookRequestDto.getCategoryId());
            bookToEdit.setCategory(category);
        } else {
            throw new EmptyInputException("Category missing");
        }

        // publisher
        if (bookRequestDto.getPublisherId() != null) {
            Publisher publisher = publisherService.getPublisher(bookRequestDto.getPublisherId());
            bookToEdit.setPublisher((publisher));
        }

        return mapper.bookToBookResponseDto(bookToEdit);
    }

    @Override
    public BookResponseDto addAuthorToBook(Long bookId, Long authorId) {
        Book book = getBook(bookId);
        Author author = authorService.getAuthor(authorId);
        if (author.getBooks().contains(author)) {
            throw new IllegalArgumentException("this author is already assigned to this book");
        }
        book.addAuthor(author);
        author.addBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto deleteAuthorFromBook(Long bookId, Long authorId) {
        Book book = getBook(bookId);
        Author author = authorService.getAuthor(authorId);
        if (!(author.getBooks().contains(book))) {
            throw new IllegalArgumentException("book does not have this author");
        }
        author.removeBook(book);
        book.deleteAuthor(author);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto addCategoryToBook(Long bookId, Long categoryId) {
        Book book = getBook(bookId);
        Category category = categoryService.getCategory(categoryId);
        if (Objects.nonNull(book.getCategory())) {
            throw new IllegalArgumentException("book already has a category");
        }
        book.setCategory(category);
        category.addBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto removeCategoryFromBook(Long bookId, Long categoryId) {
        Book book = getBook(bookId);
        Category category = categoryService.getCategory(categoryId);
        if (!(Objects.nonNull(book.getCategory()))) {
            throw new IllegalArgumentException("book does not have a category to delete");
        }
        book.setCategory(null);
        category.removeBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto addPublisherToBook(Long bookId, Long publisherId) {
        Book book = getBook(bookId);
        Publisher publisher = publisherService.getPublisher(publisherId);
        if (Objects.nonNull(book.getPublisher())) {
            throw new EntityFoundException(Book.class, "id", publisherId.toString());
        }
        book.setPublisher(publisher);
        publisher.addBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public BookResponseDto removePublisherFromBook(Long bookId, Long publisherId) {
        Book book = getBook(bookId);
        Publisher publisher = publisherService.getPublisher(publisherId);
        if (!(Objects.nonNull(book.getPublisher()))) {
            throw new EntityNotFoundException(Book.class, "id", publisherId.toString());
        }
        book.setCategory(null);
        publisher.removeBook(book);
        return mapper.bookToBookResponseDto(book);
    }

    @Override
    public String convertISBN(String s) {
        //ISBN-13 to ISBN-10
//        Boolean isIsbn13 = isValidIsbn13(s);
        try {
            if(isValidIsbn13(s)) {
                int i, k, j = 0;
                int count = 10;
                String st = s.trim().replace("-","").substring(3, 12);
                for (i = 0; i < st.length(); i++) {
                    k = Integer.parseInt(st.charAt(i) + "");
                    j = j + k * count;
                    count--;
                }
                j = (11 - (j % 11)) % 11;
                return st + "" + j;
            } else if (isValidIsbn10(s)) { //ISBN-10 to ISBN-13
                return ISBN10toISBN13(s);
            }
        } catch (Exception e) {
            return "please input numbers only";
        }
        return "invalid isbn";
    }

    public static String validateIsbn13Check(String s){
        String Isbn13 = s.trim().replace("-","").substring(0,12);
        int d;
        int sum = 0;

        for (int i = 0; i < Isbn13.length(); i++) {
            d = ((i % 2 == 0) ? 1 : 3);
            //charAT(i) will return ascii char
            //if you less 48 to a character, it will less it to its ascii code
            //Char(i)-48 will just result to its it form, just parse it if you need the number
            sum += ((((int) Isbn13.charAt(i)) - 48) * d);
//            if (LOG_D) Log.d(TAG, "adding " + ISBN13.charAt(i) + "x" + d + "=" + ((((int) ISBN13.charAt(i)) - 48) * d));
        }
        sum = 10 - (sum % 10);
        Isbn13 += sum;

        return Isbn13;
    }

    public static String validateIsbn10Check(String isbn10){
//        System.out.println(isbn10);
//        System.out.println(isbn10.trim().replace("-","").substring(0,9));
        int i,k,j=0;
        int count = 10;
        String st = isbn10.trim().replace("-","").substring(0,9);
        for (i = 0; i < st.length(); i++) {
            k = Integer.parseInt(st.charAt(i) + "");
            j = j + k * count;
            count--;
        }
        j = (11 - (j % 11)) % 11;
        return st + "" + j;
    }

    public Boolean isValidIsbn13(String s) {
        String regex = "^(?:ISBN(?:-13)?:? )?(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)97[89][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        if(matcher.matches()){
            if(!s.trim().replace("-","").equals(validateIsbn13Check(s))){
                throw new InvalidISBNException("Invalid ISBN-13");
            }
        }

        return matcher.matches();
    }

    public Boolean isValidIsbn10(String s) {
        String regex = "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        if(matcher.matches()){
            if(!s.trim().replace("-","").equals(validateIsbn10Check(s))){
                throw new InvalidISBNException("Invalid ISBN-10");
            }
        }
        return matcher.matches();
    }

    public static String ISBN10toISBN13( String ISBN10 ) {
        String ISBN13 = "";
        if(ISBN10.indexOf("-") == -1){
            ISBN13 = ISBN10.trim();
        } else {
            ISBN13  = ISBN10.replace("-","").trim();
        }

        ISBN13 = "978" + ISBN13.substring(0,9);
        //if (LOG_D) Log.d(TAG, "ISBN13 without sum" + ISBN13);
        int d;

        int sum = 0;
        for (int i = 0; i < ISBN13.length(); i++) {
            d = ((i % 2 == 0) ? 1 : 3);
            //charAT(i) will return ascii char
            //if you less 48 to a character, it will less it to its ascii code
            //Char(i)-48 will just result to its it form, just parse it if you need the number
            sum += ((((int) ISBN13.charAt(i)) - 48) * d);
//            if (LOG_D) Log.d(TAG, "adding " + ISBN13.charAt(i) + "x" + d + "=" + ((((int) ISBN13.charAt(i)) - 48) * d));
        }
        sum = 10 - (sum % 10);
        ISBN13 += sum;

        return ISBN13;
    }
}
