package com.xyzbooks.xyzbooks.services;

import com.xyzbooks.xyzbooks.dto.mapper;
import com.xyzbooks.xyzbooks.dto.requestDto.PublisherRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.PublisherResponseDto;
import com.xyzbooks.xyzbooks.exception.EntityNotFoundException;
import com.xyzbooks.xyzbooks.models.Author;
import com.xyzbooks.xyzbooks.models.Publisher;
import com.xyzbooks.xyzbooks.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PublisherServiceImpl implements PublisherService {
    private final PublisherRepository publisherRepository;

    @Autowired
    public PublisherServiceImpl(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @Override
    public Publisher getPublisher(Long publisherId) {
        return publisherRepository.findById(publisherId)
                .orElseThrow(()-> new EntityNotFoundException(Author.class, "id", publisherId.toString()));
    }

    @Override
    public PublisherResponseDto addPublisher(PublisherRequestDto publisherRequestDto) {
        Publisher publisher = new Publisher();
        publisher.setName(publisherRequestDto.getName());
        publisherRepository.save(publisher);
        return mapper.publisherToPublisherResponseDto(publisher);
    }

    @Override
    public PublisherResponseDto getPublisherById(Long publisherId) {
        Publisher publisher = getPublisher(publisherId);
        return mapper.publisherToPublisherResponseDto(publisher);
    }

    @Override
    public List<PublisherResponseDto> getPublishers() {
        List<Publisher> publishers = StreamSupport.stream(publisherRepository.findAll().spliterator(),false).collect(Collectors.toList());
        return mapper.publishersToPublisherResponseDtos(publishers);
    }

    @Override
    public PublisherResponseDto deletePublisher(Long publisherId) {
        Publisher publisher = getPublisher(publisherId);
        publisherRepository.delete(publisher);
        return mapper.publisherToPublisherResponseDto(publisher);
    }

    @Transactional
    @Override
    public PublisherResponseDto editPublisher(Long publisherId, PublisherRequestDto publisherRequestDto) {
        Publisher publisherToEdit = getPublisher(publisherId);
        publisherToEdit.setName(publisherRequestDto.getName());
        return mapper.publisherToPublisherResponseDto(publisherToEdit);
    }
}
