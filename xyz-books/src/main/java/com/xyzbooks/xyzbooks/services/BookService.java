package com.xyzbooks.xyzbooks.services;

import com.xyzbooks.xyzbooks.dto.requestDto.BookRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.BookResponseDto;
import com.xyzbooks.xyzbooks.models.Book;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface BookService {
    public BookResponseDto addBook(BookRequestDto bookRequestDto);
    public BookResponseDto getBookById(Long bookId);
    public BookResponseDto getBookByIsbn(String bookIsbn);
    public Book getBook(Long bookId);
    public Book getBookIsbn13(String bookIsbn13);
    public List<BookResponseDto> getBooks();
    public BookResponseDto deleteBook(Long bookId);
    public BookResponseDto editBook(Long bookId, BookRequestDto bookRequestDto);
    public BookResponseDto addAuthorToBook(Long bookId, Long authorId);
    public BookResponseDto deleteAuthorFromBook(Long bookId, Long authorId);
    public BookResponseDto addCategoryToBook(Long bookId, Long categoryId);
    public BookResponseDto removeCategoryFromBook(Long bookId, Long categoryId);
    public BookResponseDto addPublisherToBook(Long bookId, Long categoryId);
    public BookResponseDto removePublisherFromBook(Long bookId, Long publisherId);

    public String convertISBN(String isbn);
}
