package com.xyzbooks.xyzbooks.services;

import com.xyzbooks.xyzbooks.dto.requestDto.CategoryRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.CategoryResponseDto;
import com.xyzbooks.xyzbooks.models.Category;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {
    public Category getCategory(Long categoryId);
    public CategoryResponseDto addCategory(CategoryRequestDto categoryRequestDto);
    public CategoryResponseDto getCategoryById(Long categoryId);
    public List<CategoryResponseDto> getCategories();
    public CategoryResponseDto deleteCategory(Long categoryId);
    public CategoryResponseDto editCategory(Long categoryId, CategoryRequestDto categoryRequestDto);
}
