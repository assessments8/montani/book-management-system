package com.xyzbooks.xyzbooks.services;

import com.xyzbooks.xyzbooks.dto.requestDto.PublisherRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.PublisherResponseDto;
import com.xyzbooks.xyzbooks.models.Publisher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PublisherService {

    public Publisher getPublisher(Long publisherId);
    public PublisherResponseDto addPublisher(PublisherRequestDto publisherRequestDto);
    public PublisherResponseDto getPublisherById(Long publisherId);
    public List<PublisherResponseDto> getPublishers();
    public PublisherResponseDto deletePublisher(Long publisherId);
    public PublisherResponseDto editPublisher(Long publisherId, PublisherRequestDto publisherRequestDto);

}
