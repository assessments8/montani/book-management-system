package com.xyzbooks.xyzbooks.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String isbn13;

    @Column(nullable = false)
    @NotEmpty(message = "price required")
    private Double price;

    @Column(name = "publication_year", nullable = false)
    @NotEmpty(message = "publication year required")
    @NotNull
    private int publicationYear;


    @Column(name = "img_url")
    private String imgUrl;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private List<Author> authors = new ArrayList<>();
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    public Book(String title, String isbn13, Double price, int publicationYear, String imgUrl, List<Author> authors, Category category, Publisher publisher) {
        this.title = title;
        this.isbn13 = isbn13;
        this.price = price;
        this.publicationYear = publicationYear;
        this.imgUrl = imgUrl;
        this.authors = authors;
        this.category = category;
        this.publisher = publisher;
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    public void deleteAuthor(Author author) {
        authors.remove(author);
    }
}