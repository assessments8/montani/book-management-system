package com.xyzbooks.xyzbooks.dto;

import com.xyzbooks.xyzbooks.dto.responseDto.AuthorResponseDto;
import com.xyzbooks.xyzbooks.dto.responseDto.BookResponseDto;
import com.xyzbooks.xyzbooks.dto.responseDto.CategoryResponseDto;
import com.xyzbooks.xyzbooks.dto.responseDto.PublisherResponseDto;
import com.xyzbooks.xyzbooks.models.Author;
import com.xyzbooks.xyzbooks.models.Book;
import com.xyzbooks.xyzbooks.models.Category;
import com.xyzbooks.xyzbooks.models.Publisher;

import java.util.ArrayList;
import java.util.List;

public class mapper {
    public static BookResponseDto bookToBookResponseDto(Book book) {
        BookResponseDto bookResponseDto = new BookResponseDto();
        bookResponseDto.setId(book.getId());
        bookResponseDto.setTitle(book.getTitle());
        bookResponseDto.setIsb13(book.getIsbn13());
        bookResponseDto.setCategoryName(book.getCategory().getName());
        bookResponseDto.setPublisherName(book.getPublisher().getName());
        bookResponseDto.setPrice(book.getPrice());
        bookResponseDto.setImgUrl(book.getImgUrl());
        List<String> names = new ArrayList<>();
        List<Author> authors = book.getAuthors();
        for (Author author: authors) {
            names.add(author.getName());
        }
        bookResponseDto.setAuthorNames(names);
        return bookResponseDto;
    }

    public static List<BookResponseDto> booksToBookResponseDtos(List<Book> books) {
        List<BookResponseDto> bookResponseDtos = new ArrayList<>();
        for (Book book: books) {
            bookResponseDtos.add(bookToBookResponseDto(book));
        }
        return bookResponseDtos;
    }

    public static AuthorResponseDto authorToAuthorResponseDto(Author author) {
        AuthorResponseDto authorResponseDto = new AuthorResponseDto();
        authorResponseDto.setId(author.getId());
        authorResponseDto.setName(author.getName());
        authorResponseDto.setZipcodeName(author.getZipcode().getName());
        List<String> names = new ArrayList<>();
        List<Book> books = author.getBooks();
        for (Book book: books) {
            names.add(book.getTitle());
        }
        authorResponseDto.setBookNames(names);
        return authorResponseDto;
    }

    public static List<AuthorResponseDto> authorsToAuthorResponseDtos(List<Author> authors){
        List<AuthorResponseDto> authorResponseDtos = new ArrayList<>();
        for (Author author: authors) {
            authorResponseDtos.add(authorToAuthorResponseDto(author));
        }
        return authorResponseDtos;
    }

    public static CategoryResponseDto categoryToCategoryResponseDto(Category category) {
        CategoryResponseDto categoryResponseDto = new CategoryResponseDto();
        categoryResponseDto.setId(category.getId());
        categoryResponseDto.setName(category.getName());
        List<String> names = new ArrayList<>();
        List<Book> books = category.getBooks();
        for (Book book : books) {
            names.add(book.getTitle());
        }
        categoryResponseDto.setBookNames(names);
        return categoryResponseDto;
    }


    public static List<CategoryResponseDto> categoriesToCategoryResponseDtos(List<Category> categories) {
        List<CategoryResponseDto> categoryResponseDtos = new ArrayList<>();
        for (Category category: categories) {
            categoryResponseDtos.add(categoryToCategoryResponseDto(category));
        }
        return categoryResponseDtos;
    }


    public static PublisherResponseDto publisherToPublisherResponseDto(Publisher publisher){
        PublisherResponseDto publisherResponseDto = new PublisherResponseDto();
        publisherResponseDto.setId(publisher.getId());
        publisherResponseDto.setName(publisher.getName());
        List<String> names = new ArrayList<>();
        List<Book> books = publisher.getBooks();
        for(Book book : books){
            names.add(book.getTitle());
        }
        publisherResponseDto.setBookNames(names);
        return publisherResponseDto;
    }

    public static List<PublisherResponseDto> publishersToPublisherResponseDtos(List<Publisher> publishers){
        List<PublisherResponseDto> publisherResponseDtos = new ArrayList<>();
        for(Publisher publisher : publishers) {
            publisherResponseDtos.add(publisherToPublisherResponseDto(publisher));
        }
        return publisherResponseDtos;
    }
}
