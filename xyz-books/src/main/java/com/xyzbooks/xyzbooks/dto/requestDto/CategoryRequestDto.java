package com.xyzbooks.xyzbooks.dto.requestDto;

import lombok.Data;

import java.util.List;

@Data
public class CategoryRequestDto {
    private String name;
}
