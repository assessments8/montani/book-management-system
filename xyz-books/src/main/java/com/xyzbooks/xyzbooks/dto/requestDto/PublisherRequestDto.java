package com.xyzbooks.xyzbooks.dto.requestDto;

import lombok.Data;

@Data
public class PublisherRequestDto {
    private String name;
}
