package com.xyzbooks.xyzbooks.dto.responseDto;

import lombok.Data;

import java.util.List;

@Data
public class PublisherResponseDto {
    private Long id;
    private String name;
    private List<String> bookNames;
}
