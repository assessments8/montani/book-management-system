package com.xyzbooks.xyzbooks.dto.requestDto;

import lombok.Data;

@Data
public class CityRequestDto {
    private String name;
}
