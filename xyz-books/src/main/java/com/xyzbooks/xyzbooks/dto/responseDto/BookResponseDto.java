package com.xyzbooks.xyzbooks.dto.responseDto;

import lombok.Data;

import java.util.List;

@Data
public class BookResponseDto {
    private Long id;
    private String title;
    private String isb13;
    private Double price;
    private int publicationYear;
    private String imgUrl;
    private List<String> authorNames;
    private String categoryName;
    private String publisherName;
}
