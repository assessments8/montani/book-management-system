package com.xyzbooks.xyzbooks.dto.requestDto;

import lombok.Data;

import java.util.List;

@Data
public class BookRequestDto {
    private String title;
    private String isbn13;
    private Double price;
    private int publicationYear;
    private String imgUrl;
    private List<Long> authorIds;
    private Long categoryId;
    private Long publisherId;
}
