package com.xyzbooks.xyzbooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XyzBooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(XyzBooksApplication.class, args);
	}

}
