package com.xyzbooks.xyzbooks.controllers;

import com.xyzbooks.xyzbooks.dto.requestDto.PublisherRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.PublisherResponseDto;
import com.xyzbooks.xyzbooks.services.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/category")
public class PublisherController {

    private final PublisherService publisherService;

    @Autowired
    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @PostMapping("/add")
    public ResponseEntity<PublisherResponseDto> addPublisher(
            @RequestBody final PublisherRequestDto publisherRequestDto) {
        PublisherResponseDto publisherResponseDto = publisherService.addPublisher(publisherRequestDto);
        return new ResponseEntity<>(publisherResponseDto, HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<PublisherResponseDto> getCategory(@PathVariable final Long id) {
        PublisherResponseDto publisherResponseDto = publisherService.getPublisherById(id);
        return new ResponseEntity<>(publisherResponseDto, HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<PublisherResponseDto>> getCategories() {
        List<PublisherResponseDto> publisherResponseDtos = publisherService.getPublishers();
        return new ResponseEntity<>(publisherResponseDtos, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<PublisherResponseDto> deleteCategory(@PathVariable final Long id) {
        PublisherResponseDto publisherResponseDto = publisherService.deletePublisher(id);
        return new ResponseEntity<>(publisherResponseDto, HttpStatus.OK);
    }

    @PostMapping("/edit/{id}")
    public ResponseEntity<PublisherResponseDto> editCategory(
            @RequestBody final PublisherRequestDto publisherRequestDto,
            @PathVariable final Long id) {
        PublisherResponseDto publisherResponseDto = publisherService.editPublisher(id, publisherRequestDto);
        return new ResponseEntity<>(publisherResponseDto, HttpStatus.OK);
    }


}
