package com.xyzbooks.xyzbooks.controllers;

import com.xyzbooks.xyzbooks.dto.requestDto.BookRequestDto;
import com.xyzbooks.xyzbooks.dto.responseDto.BookResponseDto;
import com.xyzbooks.xyzbooks.exception.EmptyInputException;
import com.xyzbooks.xyzbooks.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/add")
    public ResponseEntity<BookResponseDto> addBook(@RequestBody final BookRequestDto bookRequestDto) {

        BookResponseDto bookResponseDto = bookService.addBook(bookRequestDto);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<BookResponseDto> getBook(@PathVariable final Long id) {
        BookResponseDto bookResponseDto = bookService.getBookById(id);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @GetMapping("/isbn/{isbn}")
    public ResponseEntity<BookResponseDto> getBookByIsbn(@PathVariable final String isbn) {
        BookResponseDto bookResponseDto = bookService.getBookByIsbn(isbn);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<BookResponseDto>> getBooks() {
        List<BookResponseDto> bookResponseDtos = bookService.getBooks();
        return new ResponseEntity<>(bookResponseDtos, HttpStatus.OK);
    }

    //ISBN-13 to ISBN-10
    @GetMapping("/convertIsbn/{isbn}")
    public String convertIsbn(@PathVariable final String isbn) {
        String converted = bookService.convertISBN(isbn);
        return converted;
    }

    @DeleteMapping("{id}")
    public ResponseEntity<BookResponseDto> deleteBook(@PathVariable final Long id) {
        BookResponseDto bookResponseDto = bookService.deleteBook(id);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

//    @PostMapping("/edit/{id}")
    @PutMapping("{id}")
    public ResponseEntity<BookResponseDto> editBook(@Valid @RequestBody final BookRequestDto bookRequestDto,
                                                    @PathVariable final Long id) {
        BookResponseDto bookResponseDto = bookService.editBook(id, bookRequestDto);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/addCategory/{categoryId}/to/{bookId}")
    public ResponseEntity<BookResponseDto> addCategory(@PathVariable final Long categoryId,
                                                       @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.addCategoryToBook(bookId, categoryId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/addPublisher/{publisherId}/to/{bookId}")
    public ResponseEntity<BookResponseDto> addPublisher(@PathVariable final Long publisherId,
                                                       @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.addPublisherToBook(bookId, publisherId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/removeCategory/{categoryId}/from/{bookId}")
    public ResponseEntity<BookResponseDto> removeCategory(@PathVariable final Long categoryId,
                                                          @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.removeCategoryFromBook(bookId, categoryId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/removePublisher/{publisherId}/from/{bookId}")
    public ResponseEntity<BookResponseDto> removePublisher(@PathVariable final Long publisherId,
                                                          @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.removePublisherFromBook(bookId, publisherId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/addAuthor/{authorId}/to/{bookId}")
    public ResponseEntity<BookResponseDto> addAuthor(@PathVariable final Long authorId,
                                                     @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.addAuthorToBook(bookId, authorId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }

    @PostMapping("/removeAuthor/{authorId}/from/{bookId}")
    public ResponseEntity<BookResponseDto> removeAuthor(@PathVariable final Long authorId,
                                                        @PathVariable final Long bookId) {
        BookResponseDto bookResponseDto = bookService.deleteAuthorFromBook(bookId, authorId);
        return new ResponseEntity<>(bookResponseDto, HttpStatus.OK);
    }
}



