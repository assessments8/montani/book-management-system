package com.xyzbooks.xyzbooks.advices;

import com.xyzbooks.xyzbooks.exception.EmptyInputException;
import com.xyzbooks.xyzbooks.exception.EntityNotFoundException;
import com.xyzbooks.xyzbooks.exception.InvalidISBNException;
import io.swagger.annotations.Api;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@ControllerAdvice
public class MYControllerAdvice  extends ResponseEntityExceptionHandler {
    //custom class
    @ExceptionHandler(EmptyInputException.class)
    public ResponseEntity<Object> handleEmptyInput(EmptyInputException ex){
        ApiError apiError = new ApiError(BAD_REQUEST,"EmptyInputException: Some required field is missing", ex );
        return buildResponseEntity(apiError);
    }

    //custom class
    @ExceptionHandler(InvalidISBNException.class)
    public ResponseEntity<Object> handleInvalidIsbn10Exception(InvalidISBNException ex){
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, "ISBN Format Error", ex));
    }

    //built-in class interception demo
    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<String> handleNoSuchElementException(NoSuchElementException noSuchElementException){
        return new ResponseEntity<String>("Nothing found in DB with such value", NOT_FOUND);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Object> handleNullPointerException(NullPointerException ex) {
        ApiError apiError = new ApiError(BAD_REQUEST, "Something is missing in the required field", ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(PropertyValueException.class)
    public ResponseEntity<Object> handlePropertyValueException(PropertyValueException ex){
        ApiError apiError = new ApiError(BAD_REQUEST, "PropertyValueException: Something is missing in the required field", ex);
        return buildResponseEntity(apiError);
    }

    //built-in methodException interception demo
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<Object>("Please check http method", NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
//        return super.handleHttpMessageNotReadable(ex, headers, status, request);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError){
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException ex) {
        ApiError apiError = new ApiError(NOT_FOUND);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);

    }
}
